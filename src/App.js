import React from "react";
import { Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles(theme => ({
  container: {
    backgroundColor: theme.palette.secondary.main
  }
}));

export default function App(props) {
  const classes = useStyles();

  return (
    <Typography
      className={classes.container}
      sx={{
        p: 3
      }}
      variant="h1"
    >
      Hello, world!
    </Typography>
  );
}
