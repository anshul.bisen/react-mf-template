import React from "react";
import theme from "./theme";
import { CssBaseline, ThemeProvider } from "@mui/material";
import App from "./App";

const Root = () => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <App />
    </ThemeProvider>
  );
};

export default Root;
